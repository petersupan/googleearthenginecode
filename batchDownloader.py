import sys, getopt
import datetime

import subprocess


#( beginPosition:[2017-02-01T00:00:00.000Z TO 2017-02-03T23:59:59.999Z] AND endPosition:[2017-02-01T00:00:00.000Z TO 2017-02-03T23:59:59.999Z] )

def main(begindate, enddate, assetNameStr, onlyQF):
    #begindate = datetime.datetime(2017, 02, 01, 0, 0, 0, 0);
    #enddate = datetime.datetime(2017, 03, 01, 0, 0, 0, 0);
    currentdate = begindate

    while currentdate != enddate:
        prevdate=currentdate
        currentdate += datetime.timedelta(days=1)
        beginTimeString = "T00:00:00.000Z"
        endTimeString = "T23:59:59.999Z"
        beginDateString = "{}".format(prevdate)
        endDateString = "{}".format(currentdate)

        fromdateTimeString = beginDateString+beginTimeString
        todateTimeString = endDateString+endTimeString
        timeStampString = beginDateString
        subprocString = "python downloadAndConvertToGeoTiff.py " + fromdateTimeString + " " + todateTimeString + " " +timeStampString + " " + assetNameStr + " " + onlyQF
        print(subprocString)
        subprocess.call(subprocString, shell=True)



beginString = ""
endString = ""

# try:
#     opts, args = getopt.getopt(sys.argv[1:], 'be')
# except getopt.GetoptError:
#     print 'test.py -i <inputfile> -o <outputfile>'
#     sys.exit(2)
# print(sys.argv)
# print(opts)
# for o, a in opts:
#     if o == "-b":
#         print("found b")
#         beginString = a
#     elif o== "e":
#         print("found e")
#         endString = a

beginString = sys.argv[1]
endString = sys.argv[2]

assetNameStr = sys.argv[3]
onlyQF = sys.argv[4]


bList = beginString.split("-")
eList = endString.split("-")
print(beginString)
print(bList)


beginDate = datetime.date(int(bList[0]), int(bList[1]), int(bList[2]))
endDate = datetime.date(int(eList[0]), int(eList[1]), int(eList[2]))

print(beginDate)

timestampString = "2017:03:21 00:00:00"
temp = timestampString.replace(" ", ":")
dayStrLst = temp.split(":")
dayStr = dayStrLst[0] + "_" + dayStrLst[1] + "_" +dayStrLst[2]
print(dayStr)

main(beginDate, endDate, assetNameStr, onlyQF)
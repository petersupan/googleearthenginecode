import gdal
from gdalconst import *
import sys

filename = sys.argv[1]
print(filename)
dataset = gdal.Open(filename, GA_ReadOnly)
if dataset is None:
    print("something went wrong with loading")
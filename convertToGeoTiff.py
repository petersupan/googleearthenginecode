from osgeo import gdal
import numpy
from gdalconst import *
import sys
import osr
import os

# the folder where the images lie
folder="/home/gee/PycharmProjects/GEEProjects/image/"
# the raster layers we need
rasternamelist = ["Oa08_radiance", "Oa10_radiance", "Oa11_radiance", "Oa12_radiance"]
# the metadata layers we need
metanamelist = ["geo_coordinates", "instrument_data", "quality_flags", "removed_pixels"]

# function to cut the suffix (gdal is confused by the suffix, without suffix it
# realizes there is hdf5 data in the .nc file
def cutSuffix(fn):
    filename = fn + ".nc"
    if (os.path.isfile(filename)):
        os.rename(filename, fn)
    else:
        print("could not find file: " + filename)

# go over all the names, cut the suffix if there still is one
for f in rasternamelist:
    cutSuffix(folder + f)
for f in metanamelist:
    cutSuffix(folder + f)

# open the geoCoordinates file (assumed to be the first entry in metanamelist)
geoCoordinates = gdal.Open(folder+metanamelist[0])
if geoCoordinates is None:
    print("error loading file", metanamelist[0])
else:
    print "opened file" + folder+metanamelist[0]

#creating a new file, checking GTiff driver
format = "GTiff"
driver = gdal.GetDriverByName( format )

#load raster data
desFn = folder + "test1.tif"
#load one band to get the resolution
src = gdal.Open(folder+rasternamelist[0])
if src is None:
    print("error loading file", rasternamelist[0])
else:
    print "opened file" + folder+rasternamelist[0]
    srcband = src.GetRasterBand(1)
dest = driver.Create(desFn, src.RasterXSize, src.RasterYSize, len(rasternamelist), srcband.DataType)
print(driver)
print(dest)

#loop over all input bands, add raster data to dest
for i, item in enumerate(rasternamelist, start=1):
    print "processing item index ", i
    src = gdal.Open(folder+item)
    if src is None:
        print("error loading file", item)
    else:
        print "opened file" + folder+item

    # die variante ohne numpy,
    # siehe http://geoinformaticstutorial.blogspot.co.at/2012/09/reading-raster-data-with-python-and-gdal.html

    # the numpy variant
    srcband = src.GetRasterBand(1)
    dstBand = dest.GetRasterBand(i)
    srcArray = srcband.ReadAsArray(0, 0, src.RasterXSize, src.RasterYSize)
    dstBand.WriteArray(srcArray)

#get latitude and longitude as array, generate ground control points out of these
sets=geoCoordinates.GetSubDatasets()
latSetName = sets[1][0]
lonSetName = sets[2][0]
latData = gdal.Open(latSetName, gdal.GA_ReadOnly)
lonData = gdal.Open(lonSetName, gdal.GA_ReadOnly)
proj =  latData.GetProjection()
#dest.SetProjection(proj)
gcpproj = latData.GetGCPProjection()
latBand = latData.GetRasterBand(1)
lonBand = lonData.GetRasterBand(1)
latArray = latBand.ReadAsArray(0, 0, latData.RasterXSize, latData.RasterYSize)
lonArray = lonBand.ReadAsArray(0, 0, lonData.RasterXSize, lonData.RasterYSize)

#take 9 values out of the lat long array and use them as GCP
xValues = [100, latData.RasterXSize / 2, latData.RasterXSize - 100, 100, latData.RasterXSize / 2,
           latData.RasterXSize - 100, 100, latData.RasterXSize / 2, latData.RasterXSize - 100]
yValues = [100, 100, 100, latData.RasterXSize / 2, latData.RasterXSize / 2, latData.RasterXSize / 2,
           latData.RasterYSize - 100, latData.RasterYSize - 100, latData.RasterYSize - 100]
# read the lat and long values at the 9 positions,
gcp_list = []
for index, x in enumerate(xValues):
    pixel = xValues[index]
    line = yValues[index]
    y = latArray[line][pixel] / 1000000.0
    x = lonArray[line][pixel] / 1000000.0
    z = 0.0
    # create a gcp out of the lat/long values, add to list
    gcp = gdal.GCP(x, y, z, pixel, line)
    gcp_list.append(gcp)
# set the gcps
dest.SetGCPs(gcp_list, gcpproj)

#warp image
print "warp image"
# Define target SRS
dst_srs = osr.SpatialReference()
dst_srs.ImportFromEPSG(4326)
dst_wkt = dst_srs.ExportToWkt()
error_threshold = 0.5  # error threshold --> use same value as in gdalwarp
resampling = gdal.GRA_Cubic # was gibts da noch?
tmp_ds = gdal.AutoCreateWarpedVRT( dest,
                                   None, # src_wkt : left to default value --> will use the one from source
                                   dst_wkt,
                                   resampling,
                                   error_threshold )
# Create the final warped raster
final_ds = gdal.GetDriverByName('GTiff').CreateCopy(folder+'warp_test_loerror2.tif', tmp_ds)
final_ds = None
# by setting to "None" the dest file is saved.. weird ...
dest = None
print "done"
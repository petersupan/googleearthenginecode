#import ee
import requests
from requests.auth import HTTPBasicAuth

def DownloadFile(url, userdat, local_filename):
    #local_filename = url.split('/')[-1]
    r = requests.get(url, auth=userdat, stream=True)
    total_length = r.headers.get('content-length')
    f = open(local_filename, 'wb')
    dl = 0
    for chunk in r.iter_content(chunk_size=512 * 1024): 
        if chunk: # filter out keep-alive new chunks
            f.write(chunk)
            dl += len(chunk)
            s = repr(dl) + " / " + repr(total_length)
            print(s)
    f.close()
    return


userdat=HTTPBasicAuth('petersupan', 'K/TMgyJp');
requestString = 'https://coda.eumetsat.int/search?q='
intersectString = 'footprint:%22Intersects(POLYGON((15.221 45.136,15.647 45.136,15.647 45.536,15.221 45.536,15.221 45.136)))%22'
beginPosString = 'beginPosition:[NOW-1DAYS TO NOW]'
endPosString = 'endPosition:[NOW-1DAYS TO NOW]'
pnString = '(platformname:Sentinel-3 AND producttype:OL_1_EFR___ AND instrumentshortname:OLCI)'

requestString += intersectString + ' AND ' + beginPosString + ' AND ' + endPosString + ' AND ' + pnString

# send request
resp = requests.get(requestString, auth=userdat)

# get text of xml file
xmlfileuc = resp.text
xmlfile = xmlfileuc.encode("utf8")
print(type(xmlfile))

# evil hack, just split the xml file with entry and take the first entry, get the link
entries = xmlfile.split('<entry>')
linkTagidx = entries[1].find('link')
linkTag = entries[1][linkTagidx:]
link = linkTag.split('\"')[1]
print link
# get the filename
filenameTagstartidx = entries[1].find('<str name=\"filename\">')
filenameTag = entries[1][filenameTagstartidx:]
#print filenameTag
filenameTagendidx = filenameTag.find('</str>')
filenameTag = filenameTag[:filenameTagendidx]
filename = filenameTag.split('>')[1]
print("saving link to" + filename)
DownloadFile(url=link, userdat=userdat, local_filename=filename)
#print(resp);
#print(resp.text)


from __future__ import print_function
import requests
from requests.auth import HTTPBasicAuth
import os

import sys, getopt

beginPosString = 'beginPosition:[NOW-2DAYS TO NOW]'
endPosString = 'endPosition:[NOW-2DAYS TO NOW]'
timestampString = "2017:03:21 00:00:00"
gcFilename = "warped.tif"
convertOnlyQFlags = "false"
onlyQF = False

print("I AM IN THE RIGHT FILE")

def parseCommandLine():
    beginPosString = 'beginPosition:[' + sys.argv[1] + " TO " + sys.argv[2] + "]"
    endPosString = 'endPosition:[' + sys.argv[1] + " TO " + sys.argv[2] + "]"
    timestampString = sys.argv[3] + " 00:00:00"
    print(beginPosString + " To " + endPosString)
    print(timestampString)
    gcFilename = sys.argv[4]




def DownloadFile(url, userdat, local_filename):
    # local_filename = url.split('/')[-1]
    r = requests.get(url, auth=userdat, stream=True)
    total_length = r.headers.get('content-length')
    f = open(local_filename, 'wb')
    dl = 0
    prevPercent = 0
    print("total length: " + repr(total_length))
    for chunk in r.iter_content(chunk_size=512 * 1024):

        if chunk:  # filter out keep-alive new chunks
            f.write(chunk)
            dl += len(chunk)
        percent = (dl * 100) / int(total_length)
        s = repr(percent) + "% - "
        if percent - prevPercent > 4:
            print(repr(percent) + "% - ", end=" ")
            sys.stdout.flush()
            prevPercent = percent
    f.close()
    return


#parseCommandLine()
beginPosString = 'beginPosition:[' + sys.argv[1] + " TO " + sys.argv[2] + "]"
endPosString = 'endPosition:[' + sys.argv[1] + " TO " + sys.argv[2] + "]"
timestampString = sys.argv[3] + " 00:00:00"
dateStampString = sys.argv[3]
print(beginPosString + " To " + endPosString)
print(timestampString)
gcFilename = sys.argv[4]
convertOnlyQFlags = sys.argv[5]

if 'true' == convertOnlyQFlags:
    onlyQF = True

userdat = HTTPBasicAuth('petersupan', 'K/TMgyJp');
requestString = 'https://coda.eumetsat.int/search?q='
#intersectString = 'footprint:%22Intersects(POLYGON((15.221 45.136,15.647 45.136,15.647 45.536,15.221 45.536,15.221 45.136)))%22'
intersectString = 'footprint:%22Intersects(POLYGON((15.221 45.136,15.222 45.136,15.222 45.137,15.221 45.137,15.221 45.136)))%22'
#beginPosString = 'beginPosition:[NOW-2DAYS TO NOW]'
#endPosString = 'endPosition:[NOW-2DAYS TO NOW]'
pnString = '(platformname:Sentinel-3 AND producttype:OL_1_EFR___ AND instrumentshortname:OLCI)'

requestString += intersectString + ' AND ' + beginPosString + ' AND ' + endPosString + ' AND ' + pnString
print("requestSTring: " + requestString)

# send request
resp = requests.get(requestString, auth=userdat)

# get text of xml file
xmlfileuc = resp.text
xmlfile = xmlfileuc.encode("utf8")
print(type(xmlfile))
f = open("Coda_return.xml", 'w')
f.write(xmlfile)
f.close()

# evil hack, just split the xml file with entry and take the first entry, get the link
# this is a weak spot, should really parse with the xml library and find the newest entry
entries = xmlfile.split('<entry>')
linkTagidx = entries[1].find('link')
linkTag = entries[1][linkTagidx:]
link = linkTag.split('\"')[1]
print(link)
# get the filename
filenameTagstartidx = entries[1].find('<str name=\"filename\">')
filenameTag = entries[1][filenameTagstartidx:]
# print filenameTag
filenameTagendidx = filenameTag.find('</str>')
filenameTag = filenameTag[:filenameTagendidx]
filename = filenameTag.split('>')[1]
print("filename is " + filename)
if os.path.isfile(filename):
    print("file " + filename + " already exists, do not download")
else:
    print("saving link to" + filename)
    DownloadFile(url=link, userdat=userdat, local_filename=filename)

# unzip
import zipfile

zipPath = zipfile.ZipFile(filename, 'r')
zipPath.extractall("./extracted/")
zipPath.close()

#convert to Geotiff
from osgeo import gdal
import numpy
from gdalconst import *
import sys
import osr
import os

# the folder where the images lie
folder="./extracted/"+ filename + "/"
# the raster layers we need
rasternamelist = ["Oa08_radiance", "Oa10_radiance", "Oa11_radiance", "Oa12_radiance", "Oa18_radiance", "qualityFlags"]
destRasternamelist = ["b8", "b10", "b11", "b12", "b18", "qualityFlags"]
if onlyQF:
    rasternamelist = ["qualityFlags"]
    destRasternamelist = ["qualityFlags"]

# the metadata layers we need
metanamelist = []
geoCoordinatesName = "geo_coordinates"

# function to cut the suffix (gdal is confused by the suffix, without suffix it
# realizes there is hdf5 data in the .nc file
def cutSuffix(fn):
    fullfn = fn + ".nc"
    if os.path.isfile(fullfn):
        os.rename(fullfn, fn)
    else:
        print("could not find file: " + fullfn)

# go over all the names, cut the suffix if there still is one
for f in rasternamelist:
    cutSuffix(folder + f)
for f in metanamelist:
    cutSuffix(folder + f)
cutSuffix(folder + geoCoordinatesName)

# open the geoCoordinates file (assumed to be the first entry in metanamelist)
geoCoordinates = gdal.Open(folder+geoCoordinatesName)
if geoCoordinates is None:
    print("error loading file", geoCoordinatesName)
else:
    print ("opened file" + folder+geoCoordinatesName)

#creating a new file, checking GTiff driver
format = "GTiff"
driver = gdal.GetDriverByName( format )

#load raster data
desFn = folder + "test1.tif"
#load one band to get the resolution
src = gdal.Open(folder+rasternamelist[0])
if src is None:
    print("error loading file", rasternamelist[0])
else:
    print ("opened file" + folder+rasternamelist[0])
    srcband = src.GetRasterBand(1)
dest = driver.Create(desFn, src.RasterXSize, src.RasterYSize, len(rasternamelist), srcband.DataType)
print(driver)
print(dest)

#loop over all input bands, add raster data to dest
for i, item in enumerate(rasternamelist, start=1):
    print ("processing item index "+ repr(i))
    src = gdal.Open(folder+item)
    if src is None:
        print("error loading file", item)
    else:
        print ("opened file" + folder+item)

    # die variante ohne numpy,
    # siehe http://geoinformaticstutorial.blogspot.co.at/2012/09/reading-raster-data-with-python-and-gdal.html

    # the numpy variant
    srcband = src.GetRasterBand(1)
    dstBand = dest.GetRasterBand(i)
    srcArray = srcband.ReadAsArray(0, 0, src.RasterXSize, src.RasterYSize)
    dstBand.WriteArray(srcArray)
    dstBand.SetMetadataItem("Name",destRasternamelist[i-1])
    dstBand.SetMetadataItem("name",destRasternamelist[i-1])

#get latitude and longitude as array, generate ground control points out of these
sets=geoCoordinates.GetSubDatasets()
latSetName = sets[1][0]
lonSetName = sets[2][0]
latData = gdal.Open(latSetName, gdal.GA_ReadOnly)
lonData = gdal.Open(lonSetName, gdal.GA_ReadOnly)
proj =  latData.GetProjection()
#dest.SetProjection(proj)
gcpproj = latData.GetGCPProjection()
latBand = latData.GetRasterBand(1)
lonBand = lonData.GetRasterBand(1)
latArray = latBand.ReadAsArray(0, 0, latData.RasterXSize, latData.RasterYSize)
lonArray = lonBand.ReadAsArray(0, 0, lonData.RasterXSize, lonData.RasterYSize)

#take 9 values out of the lat long array and use them as GCP
xValues = [100, latData.RasterXSize / 2, latData.RasterXSize - 100, 100, latData.RasterXSize / 2,
           latData.RasterXSize - 100, 100, latData.RasterXSize / 2, latData.RasterXSize - 100]
yValues = [100, 100, 100, latData.RasterXSize / 2, latData.RasterXSize / 2, latData.RasterXSize / 2,
           latData.RasterYSize - 100, latData.RasterYSize - 100, latData.RasterYSize - 100]
# read the lat and long values at the 9 positions,
gcp_list = []
for index, x in enumerate(xValues):
    pixel = xValues[index]
    line = yValues[index]
    y = latArray[line][pixel] / 1000000.0
    x = lonArray[line][pixel] / 1000000.0
    z = 0.0
    # create a gcp out of the lat/long values, add to list
    gcp = gdal.GCP(x, y, z, pixel, line)
    gcp_list.append(gcp)
# set the gcps
dest.SetGCPs(gcp_list, gcpproj)

# warp image
print ("warp image")
# Define target SRS
dst_srs = osr.SpatialReference()
dst_srs.ImportFromEPSG(4326)
dst_wkt = dst_srs.ExportToWkt()
error_threshold = 0.5  # error threshold --> use same value as in gdalwarp
resampling = gdal.GRA_Cubic # was gibts da noch?
if onlyQF:
    resampling = gdal.GRA_Mode
tmp_ds = gdal.AutoCreateWarpedVRT( dest,
                                   None, # src_wkt : left to default value --> will use the one from source
                                   dst_wkt,
                                   resampling,
                                   error_threshold )
# Create the final warped raster
final_ds = gdal.GetDriverByName('GTiff').CreateCopy(folder+gcFilename, tmp_ds)
# TODO: use src.GetMetadataItem("TIFFTAG_DATETIME") instead of timestampString?
# TODO: or use filename to get timestamp
final_ds.SetMetadataItem("TIFFTAG_DATETIME", timestampString)
final_ds = None
# by setting to "None" the dest file is saved.. weird ...
dest = None
print ("done saving image")

import time
import subprocess
subprocess.check_output(["gcloud", "auth", "login", "pesupan@gmail.com"])
subprocess.check_output(["gcloud", "config", "set", "project", "flhtest1-147410"])
imageToUpload=folder+gcFilename
time.sleep(5)
subprocess.check_output(["gsutil", "cp", imageToUpload, "gs://flhtest1geeassets"])


#dayStr=time.strftime("%Y_%m_%d")
temp = timestampString.replace(" ", ":")
dayStrLst = temp.split(":")
dayStr = dayStrLst[0] + "_" + dayStrLst[1] + "_" +dayStrLst[2]
#assetid="users/peSupan/autotests/Sentinel3_"+dateStampString
assetid="users/peSupan/Sentinel3/collection/"+dateStampString
if onlyQF:
    assetid = "users/peSupan/Sentinel3/qflags/" + dateStampString

argList=[]
argList.append("earthengine")
argList.append("upload")
argList.append("image")
argList.append("--asset_id="+assetid)
argList.append("--time_start=" + dateStampString)
argList.append("gs://flhtest1geeassets/" + gcFilename)
time.sleep(5)
subprocess.call(argList)

# argList = []
# argList.append("earthengine")
# argList.append("cp")
# argList.append(assetid)
# argList.append("users/peSupan/Sentinel3/collection/"+dateStampString)
# time.sleep(5)
# subprocess.call(argList)


from osgeo import gdal
from gdalconst import *
import sys

filename = sys.argv[1]

print(filename)
dataset = gdal.Open(filename, GA_ReadOnly)
if dataset is None:
    print("something went wrong with loading")

print 'Driver: ', dataset.GetDriver().ShortName,'/', \
      dataset.GetDriver().LongName
print 'Size is ',dataset.RasterXSize,'x',dataset.RasterYSize, \
      'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'
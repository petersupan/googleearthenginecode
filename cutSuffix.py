import os
import sys
import os.path

#expects filename without ".nc"
def cutSuffix(fn):
    filename = fn + ".nc"
    if (os.path.isfile(filename)):
        # create a new filename without suffix - not needed anymore
        # newfn = filename[:-3]
        # rename file
        os.rename(filename, fn)
    else:
        print("could not find file: " + filename)

filename = sys.argv[1]
cutSuffix(filename)

